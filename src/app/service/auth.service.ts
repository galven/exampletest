import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { User } from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>;
  private logInErrorSubject = new Subject<string>();

  SignUp(email:string, password:string){
        this.afAuth
            .auth
            .createUserWithEmailAndPassword(email,password)
            .then(res => console.log('Succesful sign up',res))
            .catch(
              error => window.alert(error)
              );         
  }

  getUser(){
    return this.user
  }

  Logout(){
        this.afAuth.auth.signOut().then(res =>console.log('Succesful logout',res))
        this.router.navigate(['/welcome']);
      }

  login(email:string, password:string){
            this.afAuth
                .auth.signInWithEmailAndPassword(email,password)
                .then(res => console.log('Succesful Login',res))
                .catch(
                  error => window.alert(error)
                  )
          }

          public getLoginErrors():Subject<string>{
               return this.logInErrorSubject;
             }

  constructor(public afAuth:AngularFireAuth, private router:Router) { 
    this.user = this.afAuth.authState;
  } 
}
