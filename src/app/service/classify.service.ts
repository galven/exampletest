import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  articalCollection:AngularFirestoreCollection

  private url = "https://rniqbt3fbh.execute-api.us-east-1.amazonaws.com/beta";
  
  public categories:object = {0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'}
  
  public doc:string;
  
  getCategoties(): any {
    const categoriesObservable = new Observable(observer => {
                 setTimeout(() => {
                    observer.next(this.categories);
                }, 5000);
          });
      
          return categoriesObservable;
  }

  getArticals(userId):Observable<any>{
    this.articalCollection = this.db.collection(`users/${userId}/articals`);
    return this.articalCollection.snapshotChanges().pipe(
      map(collection => collection.map(document => {
                const data = document.payload.doc.data();
                data.id = document.payload.doc.id;
                return data;
      }))
    );  
  }

  addArtical(userId:string,artical:string,classification:string){
    const artic = {artical:artical,classification:classification}
   this.userCollection.doc(userId).collection('articals').add(artic);
  }
  
  classify():Observable<any>{
    let json = {
      "articles": [
        {
          "text": this.doc
        },
      ]
    }
    let body  = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        let final = res.body.replace('[','');
        final = final.replace(']','');
        return final;      
      })
    );      
  }

  constructor(private http: HttpClient, private db:AngularFirestore,private authService:AuthService) { } 
}
