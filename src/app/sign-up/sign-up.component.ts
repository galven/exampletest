import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  email:string;
  password:string;
  private errorMessage:string;
  isError:boolean = false;

  onSubmitSignUp(){
    this.authService.SignUp(this.email,this.password);
    if(this.isError){
      this.router.navigate(['/sign-up'])
    }else{
      this.router.navigate(['/success-login'])
    }

  }

  constructor(private authService:AuthService, private router:Router) {
    this.authService.getLoginErrors().subscribe(error => {
      this.errorMessage = error;
      this.isError = true;
    });
   }

  ngOnInit() {
  }


}
