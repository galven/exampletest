import { AuthService } from './../service/auth.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../service/classify.service';
import { ImageService } from '../service/image.service';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  private categories:object = [{number:0,name:'business'},{number:1,name:'entertainment'},{number: 2,name:'politics'}, {number:3,name:'sport'}, {number:4, name:'tech'}]
  private cata:object = [{}]
  artical:string;
  classification:string;
  userId:string;

  category:string = "Loading...";
  categoryImage:string; 

  constructor(private classifyService:ClassifyService,
              public imageService:ImageService, private router:Router, private authService:AuthService) {}

  firstDropDownChanged(val: any) {
                console.log(val);
            
                if (val == "business") {
                  this.cata = ["entertainment", "politics", "sport","tech"];
                }
                else if (val == "entertainment") {
                  this.cata = ["business", "politics", "sport","tech"];
                }
                else if (val == "politics") {
                  this.cata = ["business", "entertainment", "sport","tech"];
                }else if (val == "sport") {
                  this.cata = ["business", "entertainment", "politics","tech"];
                }else
                 {
                  this.cata = [];
                }
              }

  ngOnInit() {
    this.classifyService.classify().subscribe(
      res => {
        this.category = this.classifyService.categories[res];
        this.categoryImage = this.imageService.images[res];
      }
    );
    this.authService.user.subscribe(
            user => {
              this.userId = user.uid;
             }
          )
   
  }

  saveToFirebase(){
    this.classifyService.addArtical(this.userId,this.artical,this.classification);
    this.router.navigate(['/classify']);
  }

}
