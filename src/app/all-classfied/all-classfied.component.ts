import { ClassifyService } from './../service/classify.service';
import { AuthService } from './../service/auth.service';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-all-classfied',
  templateUrl: './all-classfied.component.html',
  styleUrls: ['./all-classfied.component.css']
})
export class AllClassfiedComponent implements OnInit {


  panelOpenState = false;
  $articals:Observable<any>
  userId:string;

  constructor(private authService:AuthService, private classifyService:ClassifyService) { }

  ngOnInit() {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.$articals = this.classifyService.getArticals(this.userId); 
      }
    )
  }
}
