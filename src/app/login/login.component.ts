import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password:string;
  private errorMessage:string;
  isError:boolean = false;

  onSubmitLogin(){
    this.authService.login(this.email,this.password);   
    this.router.navigate(['/success-login']);
  }

  // checkIfError(error:boolean){
  //   if(this.isError){
  //     this.router.navigate(['/login']);
  //    }else{
  //      this.router.navigate(['/success-login']);
  //    }
 
  // }

  constructor(private authService:AuthService,
    private router:Router) { 
      this.authService.getLoginErrors().subscribe(error => {
              this.errorMessage = error;
              this.isError = true;
            });
         //  this.checkIfError(this.isError);
    }

  ngOnInit() {
  }


}
