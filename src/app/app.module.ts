import { AllClassfiedComponent } from './all-classfied/all-classfied.component';
import { ClassifyComponent } from './classify/classify.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { FormsModule }   from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import { SuccessLoginComponent } from './success-login/success-login.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { DocformComponent } from './docform/docform.component';
import { ClassifiedComponent } from './classified/classified.component';
import {AngularFireStorageModule} from '@angular/fire/storage';
import { HttpClientModule } from '@angular/common/http';
import {AngularFirestore} from '@angular/fire/firestore';
import {MatExpansionModule} from '@angular/material/expansion';


const appRoutes: Routes = [
  { path: 'all-classfied', component:AllClassfiedComponent},
  { path: 'login', component:LoginComponent},
  { path: 'sign-up', component:SignUpComponent},
  { path: 'success-login', component:SuccessLoginComponent},
  { path: 'welcome', component:WelcomeComponent},
  { path: 'classify', component:DocformComponent},
  { path: 'classified', component: ClassifiedComponent},
  { path: '', redirectTo: '/welcome', pathMatch: 'full'},
  
]

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ClassifyComponent,
    AllClassfiedComponent,
    LoginComponent,
    SignUpComponent,
    SuccessLoginComponent,
    WelcomeComponent,
    DocformComponent,
    ClassifiedComponent,
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    AngularFireStorageModule,
    FormsModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    HttpClientModule,
    MatExpansionModule,
    RouterModule,
    AngularFireModule,
    MatIconModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'exampleTest'),
    MatListModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true} // <-- debugging purposes only
    )   
  ],
  providers: [AngularFireAuth,AngularFirestore],
  bootstrap: [AppComponent]
})
export class AppModule { }
