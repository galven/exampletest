import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../service/classify.service';
import { ImageService } from '../service/image.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-docform',
  templateUrl: './docform.component.html',
  styleUrls: ['./docform.component.css']
})
export class DocformComponent implements OnInit {

  url:string;
  text:string;
  
  onSubmit(){
    this.classifyService.doc = this.text;
    this.router.navigate(['/classified']);
  }
  constructor(private classifyService:ClassifyService, 
              public imageService:ImageService,
              private router:Router ) { }

  ngOnInit() {  
  }
}
